# -*- coding: utf-8 -*-
from django.conf.urls import url

from .views import *
from .models import *
from .forms import *
from django.contrib.auth import views
from django.contrib.auth.decorators import login_required

urlpatterns = [
	url(r'^$', views.login, {'template_name': 'login.html', 'authentication_form': IniciarSesionForm, }, name="iniciar_sesion"),
    url(r'^salir$', views.logout, {'next_page': 'iniciar_sesion'}, name="cerrar_sesion"),
    #CREAR
    url(r'^crear-secretaria', CrearSecretaria.as_view(), name="crear_secretaria"),
    url(r'^crear-profesor', CrearProfesor.as_view(), name="crear_profesor"),
    url(r'^crear-operador', CrearOperador.as_view(), name="crear_operador"),
    url(r'^crear-administrador', CrearAdministrador.as_view(), name="crear_admnistrador"),
    #LISTAR
    url(r'^listar-profesores', ListarProfesores.as_view(), name="listar_profesor"),
    url(r'^listar-secretarias', ListarSecretarias.as_view(), name="listar_secretaria"),
    url(r'^listar-operadores', ListarOperadores.as_view(), name="listar_operador"),
    url(r'^listar-administradores', ListarAdministradores.as_view(), name="listar_administrador"),
    url(r'^listar-profesores', ListarProfesores.as_view(), name="listar_usuario"),
    
    #MODIFICAR
    url(r'^modificar-secretaria/(?P<pk>\d+)$$', EditarSecretaria.as_view(), name="editar_secretaria"),
    url(r'^modificar-profesor/(?P<pk>\d+)$', EditarProfesor.as_view(),name="editar_profesor"),
    url(r'^modificar-operador/(?P<pk>\d+)$', EditarOperador.as_view(), name="editar_operador"),
    url(r'^modificar-administrador/(?P<pk>\d+)$', EditarAdministrador.as_view(), name="editar_administrador"),
    
    #VISUALIZAR
    url(r'^visualizar-operador/(?P<pk>\d+)$', VisualizarOperador.as_view(), name="visualizar_operador"),
    url(r'^visualizar-profesor/(?P<pk>\d+)$', VisualizarProfesor.as_view(), name="visualizar_profesor"),
    url(r'^visualizar-secretaria/(?P<pk>\d+)$', VisualizarSecretaria.as_view(), name="visualizar_secretaria"),
    url(r'^visualizar-administrador/(?P<pk>\d+)$', VisualizarAdministrador.as_view(), name="visualizar_administrador"),
    
    url(r'^index$', login_required(Index.as_view()), name="index"),
    
]