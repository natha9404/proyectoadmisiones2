from __future__ import unicode_literals
from django.contrib.auth.models import Group, Permission
from django.db import migrations
from django.contrib.auth.management import create_permissions

def add_group_permissions(apps, schema_editor):
    for app_config in apps.get_app_configs():
        app_config.models_module = True #Puta linea!!
        create_permissions(app_config, apps=apps, verbosity=0)
    group, created = Group.objects.get_or_create(name='Administradores')
    if created:
        permissions = []
        #permissions.append(Permission.objects.get(codename="add_administrador"))
        #permissions.append(Permission.objects.get(codename="change_administrador"))
        permissions.append(Permission.objects.get(codename="add_profesor"))
        permissions.append(Permission.objects.get(codename="change_profesor"))
        permissions.append(Permission.objects.get(codename="delete_profesor"))
        permissions.append(Permission.objects.get(codename="add_secretaria"))
        permissions.append(Permission.objects.get(codename="change_secretaria"))
        permissions.append(Permission.objects.get(codename="delete_secretaria"))
        permissions.append(Permission.objects.get(codename="add_operador"))
        permissions.append(Permission.objects.get(codename="change_operador"))
        permissions.append(Permission.objects.get(codename="delete_operador"))
        permissions.append(Permission.objects.get(codename="add_criterios"))
        permissions.append(Permission.objects.get(codename="change_criterios"))
        permissions.append(Permission.objects.get(codename="delete_criterios"))
        permissions.append(Permission.objects.get(codename="add_periodos"))
        permissions.append(Permission.objects.get(codename="change_periodos"))
        permissions.append(Permission.objects.get(codename="delete_periodos"))
        permissions.append(Permission.objects.get(codename="add_group"))
        permissions.append(Permission.objects.get(codename="change_group"))
        permissions.append(Permission.objects.get(codename="delete_group"))

        for permission in permissions:
            group.permissions.add(permission)

    group, created = Group.objects.get_or_create(name='Profesores')
    if created:
        permissions = []
        #aqui van los permisos para profesores
        permissions.append(Permission.objects.get(codename="change_profesor"))
        for permission in permissions:
            group.permissions.add(permission)

   
    group, created = Group.objects.get_or_create(name='Secretarias')
    if created:
        permissions = []
        permissions.append(Permission.objects.get(codename="add_profesor"))
        permissions.append(Permission.objects.get(codename="change_profesor"))
        permissions.append(Permission.objects.get(codename="delete_profesor"))
        permissions.append(Permission.objects.get(codename="add_secretaria"))
        permissions.append(Permission.objects.get(codename="change_secretaria"))
        permissions.append(Permission.objects.get(codename="delete_secretaria"))
        permissions.append(Permission.objects.get(codename="add_operador"))
        permissions.append(Permission.objects.get(codename="change_operador"))
        permissions.append(Permission.objects.get(codename="delete_operador"))
        for permission in permissions:
            group.permissions.add(permission)
            
    group, created = Group.objects.get_or_create(name='Operadores')
    if created:
        permissions = []
        #aqui van los permisos para profesores
        permissions.append(Permission.objects.get(codename="change_operador"))
        for permission in permissions:
            group.permissions.add(permission)
            
    

class Migration(migrations.Migration):

    dependencies = [
        ('UsuarioTenant', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(add_group_permissions),
    ]