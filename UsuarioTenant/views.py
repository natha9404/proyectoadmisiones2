# -*- coding: utf-8 -*-
from .forms import *
from .models import *
from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse	
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import Group,Permission
from .permisos_usuarios import *
from django.views.generic import CreateView, ListView, UpdateView, DetailView
from .models import *
from .forms import *
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import PermissionRequiredMixin


def crear_usuario_view(request,clase_form,grupo_usuario):

	if request.method == 'POST':

		form = clase_form(request.POST)		

		if form.is_valid():			
					
			usuario = form.save()
			usuario.groups.add(Group.objects.get(name=grupo_usuario))

			return HttpResponseRedirect(reverse("listar_usuario"))

		else:
			return render(request, 'formulario_crear_usuario.html', {'form':form})

	
	form = clase_form()
	print("enviar")
	return render(request, 'formulario_crear_usuario.html', {'form':form})
	
	
class CrearProfesor (PermissionRequiredMixin, SuccessMessageMixin, CreateView):
	model = Profesor
	form_class = CrearProfesorForm
	template_name = "crear_profesor.html"
	success_url ='crear-profesor'
	succes_message = "El profesor fue creado exitosamente"
	permission_required = "add_profesor"

	def form_valid (self, form):
		profesor = form.instance
		
		self.object = form.save()
		return super(CrearProfesor, self).form_valid(form)

	def get_context_data(self, **kwargs):
		context = super(CrearProfesor, self).get_context_data(**kwargs)
		context['modificar']= False
		return context
		
	
		
		
class EditarProfesor(SuccessMessageMixin, UpdateView):
	model = Profesor
	#form_class = ModificarProfesorForm
	fields = ('primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'tipo_documento_identificacion', 'numero_documento_identificacion',  'correo_electronico', 'telefono', 'categoria', 'area_interes','estado')
	template_name = "crear_profesor.html"
	success_url = ('../listar')
	succes_message = "El profesor fue modificado exitosamente"

	def get_context_data(self, **kwargs):
		context = super(EditarProfesor, self).get_context_data(**kwargs)
		context['modificar']= True
		return context

class ListarProfesores (ListView):
	model = Profesor
	template_name = "listar_profesores.html"
	
class VisualizarProfesor(SuccessMessageMixin, DetailView):
	model = Profesor
	template_name = "detalle_profesor.html"
	
	def get_context_data(self, **kwargs):
		context = super(VisualizarProfesor, self).get_context_data(**kwargs)
		return context
	
	
class CrearSecretaria (SuccessMessageMixin, CreateView):
	model = Secretaria
	form_class = CrearSecretariaForm
	template_name = "crear_secretaria.html"
	success_url ='crear-Secretaria'
	succes_message = "La Secretaria fue creada exitosamente"

	def form_valid (self, form):
		secretaria = form.instance
		
		self.object = form.save()
		return super(CrearSecretaria, self).form_valid(form)

	def get_context_data(self, **kwargs):
		context = super(CrearSecretaria, self).get_context_data(**kwargs)
		context['modificar']= False
		return context

class ListarSecretarias (ListView):
	model = Secretaria
	template_name = "listar_secretarias.html"
	
class EditarSecretaria(SuccessMessageMixin, UpdateView):
	model = Secretaria
	fields = ('primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'tipo_documento_identificacion', 'numero_documento_identificacion',  'correo_electronico', 'telefono', 'estado')
	template_name = "crear_secretaria.html"
	success_url = ('../listar')
	succes_message = "La Secretaría fue modificado exitosamente"

	def get_context_data(self, **kwargs):
		context = super(EditarSecretaria, self).get_context_data(**kwargs)
		context['modificar']= True
		return context
		
class VisualizarSecretaria(SuccessMessageMixin, DetailView):
	model = Secretaria
	template_name = "detalle_secretaria.html"
	
	def get_context_data(self, **kwargs):
		context = super(VisualizarSecretaria, self).get_context_data(**kwargs)
		return context
	
	
class CrearOperador (SuccessMessageMixin, CreateView):
	model = Operador
	form_class = CrearOperadorForm
	template_name = "crear_operario.html"
	success_url ='crear-Operador'
	succes_message = "El operador fue creada exitosamente"

	def form_valid (self, form):
		secretaria = form.instance
		
		self.object = form.save()
		return super(CrearOperador, self).form_valid(form)

	def get_context_data(self, **kwargs):
		context = super(CrearOperador, self).get_context_data(**kwargs)
		context['modificar']= False
		return context

class ListarOperadores (ListView):
	model = Operador
	template_name = "listar_operarios.html"
	
	
class EditarOperador(SuccessMessageMixin, UpdateView):
	model = Operador
	fields = ('primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'tipo_documento_identificacion', 'numero_documento_identificacion',  'correo_electronico', 'telefono', 'estado')
	template_name = "crear_operario.html"
	success_url = ('../listar')
	succes_message = "El operario fue modificado exitosamente"

	def get_context_data(self, **kwargs):
		context = super(EditarOperador, self).get_context_data(**kwargs)
		context['modificar']= True
		return context
		
		
class VisualizarOperador(SuccessMessageMixin, DetailView):
	model = Operador
	template_name = "detalle_operador.html"
	
	def get_context_data(self, **kwargs):
		context = super(VisualizarOperador, self).get_context_data(**kwargs)
		return context
	
class CrearAdministrador (SuccessMessageMixin, CreateView):
	model = Administrador
	form_class = CrearAdministradorForm
	template_name = "crear_administrador.html"
	success_url ='crear-administrador'
	succes_message = "El Administrador fue creada exitosamente"

	def form_valid (self, form):
		secretaria = form.instance
		
		self.object = form.save()
		return super(CrearAdministrador, self).form_valid(form)

	def get_context_data(self, **kwargs):
		context = super(CrearAdministrador, self).get_context_data(**kwargs)
		context['modificar']= False
		return context
		
	def set_group(self):
		self.group = Group.objects.get(name='Administradores')

class ListarAdministradores (ListView):
	model = Administrador
	template_name = "listar_administradores.html"
	
class EditarAdministrador(SuccessMessageMixin, UpdateView):
	model = Administrador
	fields = ('primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'tipo_documento_identificacion', 'numero_documento_identificacion',  'correo_electronico', 'telefono', 'estado')
	template_name = "crear_administrador.html"
	success_url = ('../listar')
	succes_message = "El administrador fue modificado exitosamente"

	def get_context_data(self, **kwargs):
		context = super(EditarAdministrador, self).get_context_data(**kwargs)
		context['modificar']= True
		return context

class VisualizarAdministrador(SuccessMessageMixin, DetailView):
	model = Administrador
	template_name = "detalle_administrador.html"
	
	def get_context_data(self, **kwargs):
		context = super(VisualizarAdministrador, self).get_context_data(**kwargs)
		return context


class Index (SuccessMessageMixin, CreateView):
	model = Administrador
	form_class = CrearAdministradorForm
	template_name = "index2.html"