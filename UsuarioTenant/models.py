# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User
from django.core.validators import RegexValidator

from django.db import models
import re


re_alpha = re.compile('[^\W\d_]+$', re.UNICODE)
numeric = RegexValidator(r'^[0-9]*$', 'Solamente valores numericos')
alpha = RegexValidator(re_alpha, 'Solamente se reciben caracteres de A-Z')

# Create your models here.
class UsuarioTenant(User):


	IDENTIFICACION_CHOICES = (('CC','Cedula de ciudadanía'),('TI','Tarjeta de identidad'),('CE','Cedula de extranjeria'))
	ESTADO_CHOICES = (('ACTIVO','Activo'),('INACTIVO','Inactivo'))

	tipo_documento_identificacion = models.CharField(max_length=20,verbose_name="tipo de identificación",choices = IDENTIFICACION_CHOICES)
	numero_documento_identificacion = models.CharField(max_length=20,verbose_name="numero de documento de identificación", validators=[numeric])	
	correo_electronico = models.EmailField(verbose_name="correo electrónico")
	telefono = models.CharField(max_length=20,verbose_name="numero telefónico", validators=[numeric])
	estado = models.CharField(max_length=20,verbose_name="estado",default = 'ACTIVO',choices = ESTADO_CHOICES)
	primer_nombre = models.CharField(max_length=100,verbose_name="primer nombre", validators=[alpha])
	segundo_nombre = models.CharField(max_length=100,verbose_name="segundo nombre",blank=True, validators=[alpha])
	primer_apellido = models.CharField(max_length=100,verbose_name="primer apellido", validators=[alpha])
	segundo_apellido = models.CharField(max_length=100,verbose_name="segundo apellido",blank=True, validators=[alpha])
	



	def __str__(self):	
		return '%s %s %s %s' % (self.primer_nombre , self.segundo_nombre , self.primer_apellido , self.segundo_apellido)



class Administrador(UsuarioTenant):
	pass


class Operador(UsuarioTenant):
	pass

class Secretaria(UsuarioTenant):
	pass


class Profesor(UsuarioTenant):
	
	COORDINADOR_CHOICES = ( ('ACTIVO','Activo'),('INACTIVO','Inactivo') )
	CATEGORIA_CHOICES = ( ('Titular','Titular'),('Asociado','Asociado'),('Asistente','Asistente'),('Auxiliar','Auxiliar') )
		
	area_interes = models.CharField(max_length=200,verbose_name="Area de Interes", validators=[alpha])
	categoria = models.CharField(max_length=200,verbose_name="Categoria", default = 'Auxiliar', choices = CATEGORIA_CHOICES)
	coordinador = models.CharField(max_length=20,verbose_name="Coordinador",default = 'INACTIVO',choices = COORDINADOR_CHOICES)
