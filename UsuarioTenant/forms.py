# -*- coding: utf-8 -*-
from .models import *
from django import forms
from django.contrib.auth.forms import * 

class IniciarSesionForm(AuthenticationForm):

	
	class Meta:
		model = User

		username = forms.CharField(label="Usuario", max_length=30,widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
		password = forms.CharField(label="Contraseña", max_length=30,widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'password'}))



class CrearUsuarioForm(UserCreationForm):

    class Meta:
        fields = (
            'primer_nombre',
            'segundo_nombre',
            'primer_apellido',
            'segundo_apellido',
            'tipo_documento_identificacion',
            'numero_documento_identificacion',
            'correo_electronico',
            'telefono',
            'estado',
            'username',
            'password1',
            'password2',
         )
        widgets = {
        	'password1': forms.PasswordInput(),
        	'password2': forms.PasswordInput(),
        	'numero_documento_identificacion': forms.NumberInput(attrs={ 'required': 'true', 'max_length': '11' }),
        	'telefono': forms.NumberInput(attrs={ 'required': 'true', 'max_length': '12' }),
        	'primer_apellido': forms.TextInput(attrs={ 'required': 'true', 'max_length': '100' }),
        	'primer_nombre': forms.TextInput(attrs={ 'required': 'true', 'max_length': '100' }),
        	'segundo_apellido': forms.TextInput(attrs={ 'max_length': '100' }),
        	'segundo_nombre': forms.TextInput(attrs={ 'max_length': '100' }),
        	'correo_electronico': forms.EmailInput(),
    	}

        
class CrearAdministradorForm(UserCreationForm):
    class Meta:
        model = Administrador
        fields = (
            'primer_nombre',
            'segundo_nombre',
            'primer_apellido',
            'segundo_apellido',
            'tipo_documento_identificacion',
            'numero_documento_identificacion',
            'correo_electronico',
            'telefono',
            'estado',
            'username',
            'password1',
            'password2',
            
         )

        help_texts = {
            'username' : 'Requerido. 100 caracteres o menos. Letras, dígitos y @/./+/-/_  solamente.',
        }
        widgets = {
        	'password1': forms.PasswordInput(),
        	'password2': forms.PasswordInput(),
        	'numero_documento_identificacion': forms.NumberInput(attrs={ 'required': 'true', 'max_length': '11' }),
        	'telefono': forms.NumberInput(attrs={ 'required': 'true', 'max_length': '12' }),
        	'primer_apellido': forms.TextInput(attrs={ 'required': 'true', 'max_length': '100' }),
        	'primer_nombre': forms.TextInput(attrs={ 'required': 'true', 'max_length': '100' }),
        	'segundo_apellido': forms.TextInput(attrs={ 'max_length': '100' }),
        	'segundo_nombre': forms.TextInput(attrs={ 'max_length': '100' }),
        	'correo_electronico': forms.EmailInput(),
    	}
        
class CrearOperadorForm(UserCreationForm):
    class Meta:
        model = Operador
        fields = (
            'primer_nombre',
            'segundo_nombre',
            'primer_apellido',
            'segundo_apellido',
            'tipo_documento_identificacion',
            'numero_documento_identificacion',
            'correo_electronico',
            'telefono',
            'estado',
            'username',
            'password1',
            'password2',
         )

        help_texts = {
            'username' : 'Requerido. 100 caracteres o menos. Letras, dígitos y @/./+/-/_  solamente.',
        }
        widgets = {
        	'password1': forms.PasswordInput(),
        	'password2': forms.PasswordInput(),
        	'numero_documento_identificacion': forms.NumberInput(attrs={ 'required': 'true', 'max_length': '11' }),
        	'telefono': forms.NumberInput(attrs={ 'required': 'true', 'max_length': '12' }),
        	'primer_apellido': forms.TextInput(attrs={ 'required': 'true', 'max_length': '100' }),
        	'primer_nombre': forms.TextInput(attrs={ 'required': 'true', 'max_length': '100' }),
        	'segundo_apellido': forms.TextInput(attrs={ 'max_length': '100' }),
        	'segundo_nombre': forms.TextInput(attrs={'max_length': '100' }),
        	'correo_electronico': forms.EmailInput(),
    	}
        
class CrearSecretariaForm(UserCreationForm):
    class Meta:
        model = Secretaria
        fields = (
            'primer_nombre',
            'segundo_nombre',
            'primer_apellido',
            'segundo_apellido',
            'tipo_documento_identificacion',
            'numero_documento_identificacion',
            'correo_electronico',
            'telefono',
            'estado',
            'username',
            'password1',
            'password2',
         )

        help_texts = {
            'username' : 'Requerido. 100 caracteres o menos. Letras, dígitos y @/./+/-/_  solamente.',
        }
        widgets = {
        	'password1': forms.PasswordInput(),
        	'password2': forms.PasswordInput(),
        	'numero_documento_identificacion': forms.NumberInput(attrs={ 'required': 'true', 'max_length': '11' }),
        	'telefono': forms.NumberInput(attrs={ 'required': 'true', 'max_length': '12' }),
        	'primer_apellido': forms.TextInput(attrs={ 'required': 'true', 'max_length': '100' }),
        	'primer_nombre': forms.TextInput(attrs={ 'required': 'true', 'max_length': '100' }),
        	'segundo_apellido': forms.TextInput(attrs={ 'max_length': '100' }),
        	'segundo_nombre': forms.TextInput(attrs={ 'max_length': '100' }),
        	'correo_electronico': forms.EmailInput(attrs={ 'required': 'true', 'max_length': '100' }),
    	}

class CrearProfesorForm(UserCreationForm):


    class Meta:

        model = Profesor


        fields = (
            'primer_nombre',
            'segundo_nombre',
            'primer_apellido',
            'segundo_apellido',
            'tipo_documento_identificacion',
            'numero_documento_identificacion',
            'correo_electronico',
            'telefono',
            'estado',
            'area_interes',
            'categoria',
            'coordinador',
            'username',
            'password1',
            'password2',)

        help_texts = {'username' : 'Requerido. 100 caracteres o menos. Letras, dígitos y @/./+/-/_  solamente.'}
        widgets = {
        	'password1': forms.PasswordInput(),
        	'password2': forms.PasswordInput(),
        	'numero_documento_identificacion': forms.NumberInput(attrs={ 'required': 'true', 'max_length': '11' }),
        	'telefono': forms.NumberInput(attrs={ 'required': 'true', 'max_length': '12' }),
        	'primer_apellido': forms.TextInput(attrs={ 'required': 'true', 'max_length': '100'}),
        	'primer_nombre': forms.TextInput(attrs={ 'required': 'true', 'max_length': '100' }),
        	'segundo_apellido': forms.TextInput(attrs={ 'max_length': '100' }),
        	'segundo_nombre': forms.TextInput(attrs={'max_length': '100' }),
        	'correo_electronico': forms.EmailInput(),
    	}


        