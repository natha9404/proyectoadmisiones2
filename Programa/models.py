# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.db import models
from django_tenants.models import TenantMixin, DomainMixin
from django.core.validators import RegexValidator

from django.db import models
import re


re_alpha = re.compile('[^\W\d_]+$', re.UNICODE)
numeric = RegexValidator(r'^[0-9]*$', 'Solamente valores numericos')
alpha = RegexValidator(re_alpha, 'Solamente se reciben caracteres de A-Z')

ESTADOS = (('Activo', 'Activo'), ('Inactivo', 'Inactivo'))

# Create your models here.

class Tenant(TenantMixin):
    
    nombre_tenant = models.CharField(max_length=100)
    codigo_programa = models.CharField(max_length=100,verbose_name="Código del Programa Académico", validators=[numeric])
    nombre_programa = models.CharField(max_length=100,verbose_name="Nombre del Programa Académico", validators=[alpha])
    facultad = models.CharField(max_length=100,verbose_name="Facultad", validators=[alpha])
    estado = models.CharField (max_length= 8, null=False, choices=ESTADOS, verbose_name='Estado')
	
    auto_create_schema = True

    class Meta:
			ordering = ["nombre_programa"]
			verbose_name_plural = "Programas"
			verbose_name = "Programa"
			unique_together = ('nombre_tenant', 'nombre_programa')

    def __str__(self):
        return self.schema_name
        


class Domain(DomainMixin):
    pass

class Solicitud(models.Model):
    
    nombre_tenant = models.CharField(max_length=100)
    codigo_programa = models.CharField(max_length=100,verbose_name="Código del Programa Académico", validators=[numeric])
    nombre_programa = models.CharField(max_length=100,verbose_name="Nombre del Programa Académico", validators=[alpha])
    facultad = models.CharField(max_length=100,verbose_name="Facultad", validators=[alpha])
    estado_solicitud = models.CharField (max_length= 8, null=False, choices=ESTADOS, verbose_name='Estado')
    correo_electronico = models.EmailField(verbose_name="correo electrónico")
	
   

    def __str__(self):
        return self.nombre_programa