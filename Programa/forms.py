# -*- coding: utf-8 -*-
from .models import *
from django import forms
from django.contrib.auth.forms import * 
from .models import Solicitud

class CrearSolicitudForm(forms.ModelForm):

    class Meta:
        model = Solicitud
        fields = (
            'nombre_tenant',
            'codigo_programa',
            'nombre_programa',
            'facultad',
            'correo_electronico',
         )
        widgets = {
        	'codigo_programa': forms.NumberInput(attrs={ 'required': 'true', 'max_length': '10' }),
        	'nombre_tenant': forms.TextInput(attrs={ 'required': 'true', 'max_length': '50'}),
        	'nombre_programa': forms.TextInput(attrs={ 'required': 'true', 'max_length': '100' }),
        	'facultad': forms.TextInput(attrs={ 'required': 'true', 'max_length': '100' }),
        	'correo_electronico': forms.EmailInput(),
    	}