from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.views.generic import CreateView, TemplateView, ListView, UpdateView
from .models import *
from .forms import *
from django.db import connection
from UsuarioTenant.models import Administrador
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse	
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import Group,Permission
from django.http import HttpResponseRedirect
from django.shortcuts import render,get_object_or_404, get_list_or_404,redirect

# Create your views here.

class TenantCreateView(CreateView):
    model = Tenant
    fields = ['nombre_tenant', 'codigo_programa', 'nombre_programa','facultad', 'estado']
    success_url = '/registrar-tenant'

    def form_valid(self,form):
        tenant_registrado = form.instance
        tenant_registrado.schema_name = tenant_registrado.nombre_tenant
        self.object = form.save()
        dominio_tenant = Domain(domain=self.object.nombre_tenant+'.proyectoadmisiones-natha9404.c9users.io',
                                is_primary=True,
                                tenant=tenant_registrado
                                )
        dominio_tenant.save()
        connection.set_tenant(tenant_registrado)
        administrador = Administrador()
        administrador.primer_nombre = 'Administrador'
        administrador.primer_apellido = 'Defecto'
        administrador.tipo_documento_identificacion = 'CC'
        administrador.numero_documento_identificacion = '12345896'
        administrador.correo_electronico = 'admin@gmail.com'
        administrador.telefono = '47895'
        administrador.estado = 'ACTIVO'
        administrador.username = 'admin'
        administrador.password = 'pbkdf2_sha256$30000$it0nSsV84NeU$Tlsu9MZius3Zlq3pR1IILXxaCoIdYA0RUKCDflDjL6A='
        administrador.save()
        connection.set_schema_to_public()
        return super(TenantCreateView, self).form_valid(form)


class MensajesTenantsView(TemplateView):
    template_name = 'Programa/mostrar_mensajes_tenants.html'

    def get_context_data(self, **kwargs):
        context = super(MensajesTenantsView, self).get_context_data(**kwargs)
        context['mensajes'] = obtener_todos_mensajes()
        return context
    
def listar_tenants(request):
    tenants = Tenant.objects.all()
    print(tenants)
    return render(request, 'Programa/listar_programas.html', {'tenants': tenants})    
    
    
class CrearSolicitud (SuccessMessageMixin, CreateView):
	model = Solicitud
	form_class = CrearSolicitudForm
	template_name = "register_v3.html"
	success_url = ('programa/fin-registro')
	succes_message = "La solicitud fue creada exitosamente"

	def form_valid (self, form):
		solicitud = form.instance
		
		self.object = form.save()
		return super(CrearSolicitud, self).form_valid(form)
		
class ListarSolicitudes (ListView):
	model = Solicitud
	template_name = "listar_solicitudes.html"
	
class FinRegistro (SuccessMessageMixin, CreateView):
	model = Solicitud
	form_class = CrearSolicitudForm
	template_name = "register_mensaje.html"
	success_url = ('programa/listar-solicitudes')
	succes_message = "La solicitud fue creada exitosamente"
	
	
def Aprobar_solicitud (request, pk):
    solicitud = Solicitud.objects.get(id=pk)
    solicitud.estado_solicitud = 'Aprobado'
    solicitud.save()
    tenant = Tenant(schema_name = solicitud.nombre_tenant,
        nombre_tenant = solicitud.nombre_tenant,
		nombre_programa = solicitud.nombre_programa,
		codigo_programa = solicitud.codigo_programa,
		facultad = solicitud.facultad,
		estado = 'Activo',
		)
    tenant.save()
    dominio = Domain(domain = solicitud.nombre_tenant+'.proyectoadmisiones-natha9404.c9users.io', is_primary= True, tenant=tenant)
    dominio.save()
    connection.set_tenant(tenant)
    administrador = Administrador()
    administrador.primer_nombre = 'Administrador'
    administrador.primer_apellido = 'Defecto'
    administrador.tipo_documento_identificacion = 'CC'
    administrador.numero_documento_identificacion = '12345896'
    administrador.correo_electronico = 'admin@gmail.com'
    administrador.telefono = '47895'
    administrador.estado = 'ACTIVO'
    administrador.username = 'admin'
    administrador.password = 'pbkdf2_sha256$30000$it0nSsV84NeU$Tlsu9MZius3Zlq3pR1IILXxaCoIdYA0RUKCDflDjL6A='
    administrador.save()
    connection.set_schema_to_public()
    return redirect('listar_solicitudes')
    
    


	
	
