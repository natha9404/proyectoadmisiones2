from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', CrearSolicitud.as_view(), name="crear_solicitud"),
    url(r'^listar$',listar_tenants,name = "listarprogramas"),
    url(r'^crear-solicitud', CrearSolicitud.as_view(), name="crear_solicitud"),
    url(r'^listar-solicitudes', ListarSolicitudes.as_view(), name="listar_solicitud"),
    url(r'^fin-registro', FinRegistro.as_view(), name="fin_registro"),
    url(r'^aprobar-solicitud/(?P<pk>\d+)$', Aprobar_solicitud, name="aprobar_solicitud"),
    
    
    
]