from django.conf.urls import url
from .views import CriteriosCreateView, CriteriosListView, CriteriosUpdateView, CriteriosDetailview, CriterioStateUpdateView

urlpatterns = [
    url(r'^crear-criterio$', CriteriosCreateView.as_view(), name='crear_criterio'),
    url(r'^listar-criterios$', CriteriosListView.as_view(), name='listar_criterios'),
    url(r'^detallar-criterio/(?P<pk>\d+)$', CriteriosDetailview.as_view(), name='detallar_criterio'),
    url(r'^modificar-criterio/(?P<pk>\d+)$', CriteriosUpdateView.as_view(), name='modificar_criterio'),
    url(r'^cambioEstado_criterio/(?P<pk>\d+)$', CriterioStateUpdateView.as_view(), name='cambioEstado_criterio'),
    ]
