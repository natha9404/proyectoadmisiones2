from django.apps import AppConfig


class CriteriosEvaluacionConfig(AppConfig):
    name = 'criterios_evaluacion'
