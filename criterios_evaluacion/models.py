# -*- coding: utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse
from Periodos.models import Periodos

class Criterios(models.Model):
    ESTADO_CHOICES = (('ACTIVO','Activo'),('INACTIVO','Inactivo'))

    nombre = models.CharField(verbose_name="Nombre del criterio", max_length=40)
    peso = models.IntegerField(verbose_name="Valor porcentual para el criterio")
    periodo = models.ForeignKey(Periodos, on_delete=models.CASCADE, verbose_name = "Periodo al que pertenece el criterio")
    estado = models.CharField(max_length=20,verbose_name="estado",default = 'ACTIVO',choices = ESTADO_CHOICES)

    def __str__(self):
        return '%s %s %s' % (self.nombre, self.peso, "Porciento")