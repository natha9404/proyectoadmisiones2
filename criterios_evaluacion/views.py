# -*- coding: utf-8 -*-
from django.views.generic import CreateView, UpdateView, DetailView,ListView
from .models import Criterios
from .forms import CriteriosCreateForm, CriteriosUpdateForm
from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from utilities.datatables_tools.datatables_tools import DatatablesListView
from operator import eq
from django.contrib.messages.views import SuccessMessageMixin


class CriteriosCreateView(CreateView):
    success_msg = "Criterios Creado"
    model = Criterios
    form_class = CriteriosCreateForm
    template_name = "gestion_criterios/criterios_form.html"
    #permission_required = "gestion_curso.add_curso"

    def form_valid(self, form):
        messages.success(self.request, self.success_msg)
        return super(CriteriosCreateView, self).form_valid(form)

class CriteriosUpdateView(SuccessMessageMixin, UpdateView):
	model = Criterios
	orm_class = CriteriosCreateForm
	fields = ('nombre', 'peso', 'estado')
	
	template_name = "gestion_criterios/criterios_form.html"
	success_url = ('../listar-criterios')
	succes_message = "El criterio fue modificado exitosamente"

	def get_context_data(self, **kwargs):
		context = super(CriteriosUpdateView, self).get_context_data(**kwargs)
		context['modificar']= True
		return context

class CriteriosListView(ListView):
    model = Criterios
    template_name = 'gestion_criterios/criterios_list.html'
    

class CriteriosDetailview(DetailView):
    model = Criterios
    template_name = 'gestion_criterios/criterio_detalle.html'


class CriterioStateUpdateView(UpdateView):
    success_msg = "Estado Actualizado"
    model = Criterios

    def get(self, *args, **kwargs):
        Criterios = get_object_or_404(self.model, pk=kwargs['pk'])
        Criterios.estado = not Criterios.estado
        Criterios.save()
        messages.success(self.request, self.success_msg)
        return redirect(Criterios.get_absolute_url())

    #permission_required = 'gestion_criterios.change_Criterios'