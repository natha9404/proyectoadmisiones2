from django import forms
from .models import Criterios

class CriteriosCreateForm(forms.ModelForm):

    class Meta:
        model = Criterios
        fields = ('nombre', 'peso', 'periodo')


class CriteriosUpdateForm(forms.ModelForm):

    class Meta:
        model = Criterios
        fields = ('nombre', 'peso', 'periodo')