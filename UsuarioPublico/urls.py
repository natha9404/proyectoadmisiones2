# -*- coding: utf-8 -*-
from django.conf.urls import url

from .views import *
from .models import *
from .forms import *
from django.contrib.auth import views

urlpatterns = [
    
	url(r'^ingresar/?$', views.login, {'template_name': 'login.html', 'authentication_form': IniciarSesionForm}, name="iniciar_sesion"),
    url(r'^salir$', views.logout, {'next_page': 'iniciar_sesion'}, name="cerrar_sesion"),
    url(r'^crear$', crear_usuario_view, {'clase_form':CrearAdministradorForm,'grupo_usuario':'Administrador'}, name="crear_usuario"),
    url(r'^modificar/(?P<id_usuario>\d+)$', modificar_usuario_view,{'clase_form':CrearAdministradorForm,'modelo':Administrador}, name="modificar_usuario"),
    url(r'^eliminar/(?P<id_usuario>\d+)$', eliminar_usuario_view,{'clase_form':CrearAdministradorForm,'modelo':Administrador}, name="eliminar_usuario"), 
    url(r'^activar/(?P<id_usuario>\d+)$', activar_usuario_view,{'clase_form':CrearAdministradorForm,'modelo':Administrador}, name="activar_usuario"), 

 
# #    url(r'^consultar$', consultar_usuario_view, name="consultar_usuario"),

    url(r'^listar$', listar_usuario_view, name="listar_usuario"),
    url(r'^permisos$', configurar_permisos_view, name="configurar_permisos"),
    url(r'^profile/(?P<id_usuario>\d+)$', profile_usuario_view, name="profile_usuario"),
    url(r'^index$', Index.as_view(), name="index"),
    
    
    
]