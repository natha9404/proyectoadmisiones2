# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-15 19:26
from __future__ import unicode_literals

from django.conf import settings
import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('tipo_documento_identificacion', models.CharField(choices=[('CC', 'Cedula de ciudadan\xeda'), ('TI', 'Tarjeta de identidad'), ('CE', 'Cedula de extranjeria')], max_length=20, verbose_name='tipo de identificaci\xf3n')),
                ('numero_documento_identificacion', models.CharField(max_length=20, verbose_name='numero de documento de identificaci\xf3n')),
                ('correo_electronico', models.EmailField(max_length=254, verbose_name='correo electr\xf3nico')),
                ('telefono', models.CharField(max_length=20, verbose_name='numero telef\xf3nico')),
                ('estado', models.CharField(choices=[('ACTIVO', 'Activo'), ('INACTIVO', 'Inactivo')], default='ACTIVO', max_length=20, verbose_name='estado')),
                ('primer_nombre', models.CharField(max_length=100, verbose_name='primer nombre')),
                ('segundo_nombre', models.CharField(blank=True, max_length=100, verbose_name='segundo nombre')),
                ('primer_apellido', models.CharField(max_length=100, verbose_name='primer apellido')),
                ('segundo_apellido', models.CharField(blank=True, max_length=100, verbose_name='segundo apellido')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Administrador',
            fields=[
                ('usuario_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='UsuarioPublico.Usuario')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            bases=('UsuarioPublico.usuario',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
