# -*- coding: utf-8 -*-
from .forms import *
from .models import *
from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse	
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import Group,Permission
from .permisos_usuarios import *
from django.views.generic import CreateView, ListView, UpdateView
from .models import *
from .forms import *
from django.contrib.messages.views import SuccessMessageMixin


def configurar_permisos_view(request):

	grupo_administrador, grupo_administrador_creado = Group.objects.get_or_create(name="Administrador")
	

	permisos_administrador = Permission.objects.filter(codename__in=PERMISOS_ADMINISTRADOR)
	grupo_administrador.permissions.set(permisos_administrador)

	return HttpResponse("Permisos Actualizados")
	
@login_required()
def crear_usuario_view(request,clase_form,grupo_usuario):

	if request.method == 'POST':

		form = clase_form(request.POST)		

		if form.is_valid():			
					
			usuario = form.save()
			usuario.groups.add(Group.objects.get(name=grupo_usuario))

			return HttpResponseRedirect(reverse("listar_usuario"))

		else:
			return render(request, 'formulario_crear_usuario.html', {'form':form})

	
	form = clase_form()
	print("enviar")
	return render(request, 'formulario_crear_usuario.html', {'form':form})


@login_required()
def listar_usuario_view(request):
	lista = User.objects.all()
	
	print(lista)
	return render(request, 'lista_usuarios.html', {'lista':lista})
	
@login_required()
def modificar_usuario_view(request,id_usuario,clase_form,modelo):
	
	usuario = get_object_or_404(modelo,id=id_usuario)
	if request.method == 'POST':

		form = clase_form(request.POST,instance=usuario)		

		if form.is_valid():
			form.save()			
			return HttpResponseRedirect(reverse("listar_usuario"))
		
	form = clase_form(instance=usuario)
	
	return render(request, 'formulario_crear_usuario.html', {'form':form})
	
	
def consultar_usuario_view(request):

	return render(request, 'prueba.html', {})
	
	
def profile_usuario_view(request, id_usuario):
	usuario = get_object_or_404(Usuario,id = id_usuario)

	
	return render(request, 'perfil_usuario.html', {'usuario':usuario})

@login_required()
def eliminar_usuario_view(request,id_usuario,clase_form,modelo):
	usuario = get_object_or_404(Usuario,id = id_usuario)

	usuario.estado = 'INACTIVO'
	usuario.save()

	##usuario.delete()
	return listar_usuario_view(request)
	
@login_required()
def activar_usuario_view(request,id_usuario,clase_form,modelo):
	usuario = get_object_or_404(Usuario,id = id_usuario)

	usuario.estado = 'ACTIVO'
	usuario.save()

	return listar_usuario_view(request)
	
	
class Index (SuccessMessageMixin, CreateView):
	model = Administrador
	form_class = CrearAdministradorForm
	template_name = "index.html"
	
