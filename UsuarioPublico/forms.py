# -*- coding: utf-8 -*-
from .models import *
from django import forms
from django.contrib.auth.forms import * 

class IniciarSesionForm(AuthenticationForm):

	
	class Meta:
		model = User

		username = forms.CharField(label="Usuario", max_length=30,widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
		password = forms.CharField(label="Contraseña", max_length=30,widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'password'}))

class CrearUsuarioForm(UserCreationForm):

    class Meta:
        fields = (
            'primer_nombre',
            'segundo_nombre',
            'primer_apellido',
            'segundo_apellido',
            'tipo_documento_identificacion',
            'numero_documento_identificacion',
            'correo_electronico',
            'telefono',
            'estado',
            'username',
            'password1',
            'password2',
         )

        
class CrearAdministradorForm(UserCreationForm):
    class Meta:
        model = Administrador
        fields = (
            'primer_nombre',
            'segundo_nombre',
            'primer_apellido',
            'segundo_apellido',
            'tipo_documento_identificacion',
            'numero_documento_identificacion',
            'correo_electronico',
            'telefono',
            'estado',
            'username',
            'password1',
            'password2',
         )

        help_texts = {
            'username' : 'Requerido. 150 caracteres o menos. Letras, dígitos y @/./+/-/_  solamente.',
        }
