# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User

from django.db import models

# Create your models here.
ESTADOS = (('Activo', 'Activo'), ('Inactivo', 'Inactivo'))


class Usuario(User):

	IDENTIFICACION_CHOICES = (('CC','Cedula de ciudadanía'),('TI','Tarjeta de identidad'),('CE','Cedula de extranjeria'))
	ESTADO_CHOICES = (('ACTIVO','Activo'),('INACTIVO','Inactivo'))

	tipo_documento_identificacion = models.CharField(max_length=20,verbose_name="tipo de identificación",choices = IDENTIFICACION_CHOICES)
	numero_documento_identificacion = models.CharField(max_length=20,verbose_name="numero de documento de identificación")	
	correo_electronico = models.EmailField(verbose_name="correo electrónico")
	telefono = models.CharField(max_length=20,verbose_name="numero telefónico")
	estado = models.CharField(max_length=20,verbose_name="estado",default = 'ACTIVO',choices = ESTADO_CHOICES)
	primer_nombre = models.CharField(max_length=100,verbose_name="primer nombre")
	segundo_nombre = models.CharField(max_length=100,verbose_name="segundo nombre",blank=True)
	primer_apellido = models.CharField(max_length=100,verbose_name="primer apellido")
	segundo_apellido = models.CharField(max_length=100,verbose_name="segundo apellido",blank=True)
	



	def __str__(self):	
		return '%s %s %s %s' % (self.primer_nombre , self.segundo_nombre , self.primer_apellido , self.segundo_apellido)

class Administrador(Usuario):
	pass

