# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-26 22:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Periodos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_inicio', models.DateField()),
                ('fecha_fin', models.DateField()),
                ('nombre_periodo', models.CharField(max_length=40, verbose_name='Nombre del periodo academico')),
                ('cupos', models.PositiveIntegerField(default=0)),
                ('fecha_creacion', models.DateField(auto_now_add=True)),
                ('estado', models.CharField(choices=[('ACTIVO', 'Activo'), ('INACTIVO', 'Inactivo')], default='ACTIVO', max_length=20, verbose_name='estado')),
            ],
        ),
    ]
