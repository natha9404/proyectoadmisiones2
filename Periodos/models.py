from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Periodos(models.Model):
    ESTADO_CHOICES = (('ACTIVO','Activo'),('INACTIVO','Inactivo'))
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()
    nombre_periodo = models.CharField(verbose_name="Nombre del periodo academico", max_length=40)
    cupos = models.PositiveIntegerField(default=0)
    fecha_creacion = models.DateField(auto_now_add=True, blank=True)
    estado = models.CharField(max_length=20,verbose_name="estado",default = 'ACTIVO',choices = ESTADO_CHOICES)


    def __str__(self):
        return str(self.nombre_periodo)