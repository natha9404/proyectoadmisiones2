# -*- coding: utf-8 -*-
from .forms import *
from .models import *
from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse	
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import Group,Permission
from django.views.generic import CreateView, ListView, UpdateView
from .models import *
from .forms import *
from django.contrib.messages.views import SuccessMessageMixin

# Create your views here.

class CrearPeriodos (SuccessMessageMixin, CreateView):
	model = Periodos
	form_class = CrearPeriodosForm
	template_name = "crear_periodo.html"
	success_url ='listar-periodos'
	succes_message = "El Periodo fue creado exitosamente"

	def form_valid (self, form):
		periodos = form.instance
		
		self.object = form.save()
		return super(CrearPeriodos, self).form_valid(form)
		
class EditarPeriodo(SuccessMessageMixin, UpdateView):
	model = Periodos
	orm_class = CrearPeriodosForm
	fields = ('fecha_inicio', 'fecha_fin','cupos','estado')
	
	template_name = "crear_periodo.html"
	success_url = ('../listar-periodos')
	succes_message = "El periodo fue modificado exitosamente"

	def get_context_data(self, **kwargs):
		context = super(EditarPeriodo, self).get_context_data(**kwargs)
		context['modificar']= True
		return context
		
class ListarPeriodos(ListView):
	model = Periodos
	template_name = "listar_periodos.html"
