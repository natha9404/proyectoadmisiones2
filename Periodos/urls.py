# -*- coding: utf-8 -*-
from django.conf.urls import url

from .views import *
from .models import *
from .forms import *
from django.contrib.auth import views


urlpatterns = [
    url(r'^crear-periodos', CrearPeriodos.as_view(), name="crear_periodo"),
    url(r'^modificar-periodos/(?P<pk>\d+)$', EditarPeriodo.as_view(), name="modificar_periodo"),
    url(r'^listar-periodos', ListarPeriodos.as_view(), name="listar_periodos"),
    
    ]