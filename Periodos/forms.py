# -*- coding: utf-8 -*-
from .models import *
from django import forms
from django.contrib.auth.forms import * 
from .models import Periodos

class CrearPeriodosForm(forms.ModelForm):

    class Meta:
        model = Periodos
        fields = (
            'fecha_inicio',
            'fecha_fin',
            'nombre_periodo',
            'cupos',
            'estado',
         )