echo ""
for directory in *; do
    if [ -d $directory/manual_migrations ]; then
        echo "Realizando migraciones manuales de ${directory}"
        for file in $directory/manual_migrations/*; do
            cp $(pwd)/$file $(pwd)/$directory/migrations
            python manage.py migrate_schemas --settings=proyectoadmisiones.settings
        done
    fi
done
echo "Migraciones manuales terminadas"

