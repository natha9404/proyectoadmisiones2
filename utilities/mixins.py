from django.contrib import messages
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache


class MessageMixin(object):

    # Mensajes por defecto
    mensaje_exito = "La información se ha guardo exitosamente!"
    mensaje_error = "El formulario tiene errores, por favor revise la información."

    def form_valid(self, form):
        messages.success(self.request, self.mensaje_exito)
        return super(MessageMixin, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, self.mensaje_error)
        return super(MessageMixin, self).form_invalid(form)


class NeverCacheMixin(object):
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(NeverCacheMixin, self).dispatch(*args, **kwargs)