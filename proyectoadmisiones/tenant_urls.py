from django.conf.urls import url,include
from django.contrib import admin
from Mensajes.views import MensajeCreateView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^registrar-mensaje/$', MensajeCreateView.as_view()),
    url(r'^prueba/', include('prueba.urls')),
    url(r'^usuario/', include('UsuarioTenant.urls')),
    url(r'^criterios/', include('criterios_evaluacion.urls')),
    url(r'^periodos/', include('Periodos.urls')),
    url(r'^$', include('UsuarioTenant.urls')),
    
]
