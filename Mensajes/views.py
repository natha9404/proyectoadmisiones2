from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.views.generic import CreateView, TemplateView
from .models import Mensaje

# Create your views here.
class MensajeCreateView(CreateView):
    model = Mensaje
    fields = ['cuerpo_mensaje']
    success_url = '/registrar-mensaje'

    def form_valid(self,form):
        mensaje_registrado = form.instance
        mensaje_registrado.schema_name = mensaje_registrado.cuerpo_mensaje
        self.object = form.save()
        return super(MensajeCreateView, self).form_valid(form)