from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Mensaje(models.Model):
    cuerpo_mensaje = models.TextField(max_length=300)
